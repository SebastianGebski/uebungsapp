Rails.application.routes.draw do
  root             'static_pages#home'
  get 'help'    => 'static_pages#help'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]

  resources :translinks
  resources :demandsites
  resources :supplysites
  resources :sites

  get 'transport_start', to: 'static_pages#transport_start'


  post 'translinks/read_and_show_ofv', :to => 'translinks#read_and_show_ofv'
  post 'translinks/read_transportation_quantities', :to => 'translinks#read_transportation_quantities'
  post 'translinks/optimize', :to => 'translinks#optimize'
  post 'translinks/delete_transportation_quantities', :to => 'translinks#delete_transportation_quantities'


end
